import pandas as pd
import openpyxl
import flask

from flask import Flask, url_for, make_response, request, render_template
from flask_ngrok import run_with_ngrok

app = Flask(__name__)
app.secret_key = b'_5#y2L"F4Q8z\n\xec]/'
run_with_ngrok(app)

@app.route('/')
@app.route('/index')
def index():
    return render_template('beginn.html')
    
@app.route("/req", methods = ['POST'])
def req(): 
    genre = request.form.getlist('genre')  
    country = request.form.getlist('country')
    min_year = request.form.get('min_year')
    max_year = request.form.get('max_year')
    
    wb = openpyxl.load_workbook(filename = 'newdb.xlsx')
    
    
    if 'result' in wb.sheetnames:
        wb.remove(wb['result'])
    wb.create_sheet('result')
    wb.save('newdb.xlsx')
    print(min_year)
    print(max_year)
    
    sheetInfo = wb['movie_info']
    sheetResult = wb['result']
    
    def valueKey(cell, i):
        return cell + str(i);
    
    def genreKey(i):
        return valueKey('C', i)
    
    def checkGenre(i):
        if not genre:
            return True;
        
        filmGenres = sheetInfo[genreKey(i)].value
        
        if not filmGenres:
            return False;
        
        return genre and (any(g.lower() in filmGenres for g in genre))
    
    def countryKey(i):
        return valueKey('G', i)
    
    def checkCountry(i):
        if not country:
            return True;
        
        filmCountries = sheetInfo[countryKey(i)].value
        
        if not filmCountries:
            return False;
        
        return country and (any(c in filmCountries for c in country))
    
    def yearKey(i):
        return valueKey('F', i)
    
    def ratingKey(i):
        return valueKey('I', i)
    
    def Years(i):   
        filmYears = []
        filmYear = sheetInfo[yearKey(i)].value
        if filmYear in range(int(min_year), int(max_year), 1):
            filmYears = filmYear
        print(filmYears)
        return filmYears 
    
    def filmNameKey(i):
        return valueKey('D', i)
    
    def filterFilms():
        resultRowIndex = 1
        max_row = sheetInfo.max_row
        for i in range(2, max_row+1):
            if checkGenre(i) and checkCountry(i) and Years(i): 
                sheetResult.cell(row=resultRowIndex, column=1).value = sheetInfo[filmNameKey(i)].value + ", " + str(sheetInfo[yearKey(i)].value) + " (рейтинг : " + sheetInfo[ratingKey(i)].value + ")"
                resultRowIndex += 1
    
    def getFilms():
        max_row = sheetResult.max_row
        value = [];
        for i in range(1,max_row+1):
            key = 'A' + str(i)
            sheetValue = sheetResult[key].value
            if sheetValue:
                value.append(sheetValue)
        return ';'.join(value);
                
    filterFilms()
    wb.save('newdb.xlsx')
    res = make_response(getFilms())
    
    wb.remove(wb['result'])
    wb.save('newdb.xlsx')
    
    return render_template("result.html", films=getFilms(), genre=', '.join(genre), country=', '.join(country))
app.run()